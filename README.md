# EnableCoffee app

## About

Just an example of a web application to remotely control the warming of a coffee machine using a RPi 3, and a web application for the Internet of things made with Python and Flask framework.

## Author

Anthony Lister [Fairfield Labs](https://anthlis.io)

## Things you will need

- A Raspberry Pi 3 with Raspbian Jessie loaded.
- Some sensors/leds/whatever to play with.

## Usage

Be sure that your Raspberry Pi has the `RPi.GPIO` library installed:

```shell
pi@raspberrypi:~ $ apt-get update
pi@raspberrypi:~ $ apt-get install python-rpi.gpio python3-rpi.gpio
```

or using `pip`:

```shell
pi@raspberrypi:~ $ pip install RPi.GPIO
```

And then install `Flask` by executing:

```shell
pi@raspberrypi:~ $ pip install Flask
```

In the user folder on your Raspberry Pi (or wherever you want), clone this repo:

```shell
pi@raspberrypi:~ $ git clone https://anthlis@bitbucket.org/anthlis/enablecoffee.git
```

Wired all the things properly, and then execute the web application by typing:

```shell
pi@raspberrypi:~/enablecoffee $ python main.py
```

And then go to your web browser and enters the URL `http://your-raspberrypi-ip:5000` you'll see what is going on.

## License

**EnableCoffee** is released under the [MIT License](http://www.opensource.org/licenses/MIT).
